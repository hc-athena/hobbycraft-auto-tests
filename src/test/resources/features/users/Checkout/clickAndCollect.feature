Feature: Click and collect journey

#Note the price of products, as there is a minimum spend for click and collect.
#Ensure the "number"

  @Regression
  Scenario Outline: Click and Collect not met minimum spend
    Given I am on the Hobbycraft home page
    And Choose my preferred store with <postcode>
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for click and collect
    And Check minimum spend alert appears

    Examples:
      | postcode  | search_1               | product_1                                 |
      | "LS1 6JL" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" |

  @Smoke @Regression
  Scenario Outline: Click and Collect with registered user (using store button)
    Given I am on the Hobbycraft home page
    And Choose my preferred store with <postcode>
    When I search for <search_1>
    And Select <product_1>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for click and collect
    And Checkout as registered user with <username> and <password>
    And Click save and continue
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order for click and collect

    Examples:
      | postcode  | search_1               | product_1                                 | number | username                        | password           | card_number        | expiry_date | CVC   | name     | first_name | last_name | mobile_number | address_1        | address_2      | town    |
      | "LS1 6JL" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "3"    | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "4444333322221111" | "03/30"     | "737" | "T Test" | "Test"     | "Test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" |

  @Regression
  Scenario Outline: Click and Collect with guest checkout (using store button)
    Given I am on the Hobbycraft home page
    And Choose my preferred store with <postcode>
    When I search for <search_1>
    And Select <product_1>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for click and collect
    And Checkout as guest with <email>
    And Fill in collection details with <first_name>, <last_name> and <mobile_number>
    And Click save and continue
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order for click and collect

    Examples:
      | postcode  | search_1               | product_1                                 | number | email                        | first_name | last_name | mobile_number | card_number        | expiry_date | CVC   | name     | address_1        | address_2      | town    |
      | "LS1 6JL" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "3"    | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "4444333322221111" | "03/30"     | "737" | "T Test" | "5 Albion Place" | "Albion Court" | "Leeds" |

@Ignore
  Scenario Outline: Click and Collect with guest checkout using find store prior to basket page
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Select store with <postcode>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for click and collect
    And Checkout as guest with <email>
    And Fill in collection details with <first_name>, <last_name> and <mobile_number>
    And Click save and continue
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order for click and collect

    Examples:
      | search_1               | product_1                                 | number | email                         | first_name | last_name | mobile_number | card_number        | expiry_date | CVC   | name     | address_1        | address_2      | town    | postcode  |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "3"    | "olivia.quickenden@ten10.com" | "test"     | "test"    | "12345678910" | "4444333322221111" | "03/30"     | "737" | "T Test" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" |

@Ignore
  Scenario Outline: Click and Collect with guest checkout with PayPal
    Given I am on the Hobbycraft home page
    And Choose my preferred store with <postcode>
    When I search for <search_1>
    And Select <product_1>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for click and collect
    And Checkout as guest with <email>
    And Fill in collection details with <first_name>, <last_name> and <mobile_number>
    And Click save and continue
    Then Select pay by PayPal
    And Login to PayPal with <payPalUsername> and <payPalPassword>
    Then Complete order for PayPal for click and collect

    Examples:
      | search_1               | product_1                                 | number | email                        | first_name | last_name | mobile_number | postcode  | payPalUsername          | payPalPassword    |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "3"    | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "LS1 6JL" | "calum.brown@ten10.com" | "Hobbycraft2021!" |



    # #Will add to regression pack once "Click find a store" button fixed.
##@Regression
#  @Ignore
#  Scenario Outline: Click and Collect with guest checkout
#    Given I am on the Hobbycraft home page
#    When I search for <search_1>
#    And Select <product_1>
#    And Increase product quantity with <number>
#    And Add increased quantity to basket from PDP
#    And Select view basket for click and collect
#    #Click find a store button not currently clickable.
#    And Click find a store
#    And Checkout as guest with <email>
#    And Fill in collection details with <first_name>, <last_name> and <mobile_number>
#    And Click save and continue
#    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
#    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
#    Then Complete order
#
#    Examples:
#      | search_1                               | product_1                                           | number | email                         | first_name | last_name | mobile_number | card_number        | expiry_date | CVC   | name     | address_1        | address_2      | town    | postcode  |
#      | "White Modelling Clay" | "DMC Black Gold Diamant Metallic Thread 35m (D140)" | "5"    | "olivia.quickenden@ten10.com" | "test"     | "test"    | "12345678910" | "4444333322221111" | "03/30"     | "737" | "T Test" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" |
