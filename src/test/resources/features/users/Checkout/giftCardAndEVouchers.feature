Feature: Journeys using gift cards and e-vouchers

  @Regression
  Scenario Outline: Add random item to basket and checkout as registered user using Gift Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in gift card details with <gift_card_number> and <pin>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1               | product_1                                 | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | gift_card_number   | pin   |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "9876540651439893" | "760" |

  @Regression
  Scenario Outline: Click and Collect with guest checkout with Gift card
    Given I am on the Hobbycraft home page
    And Choose my preferred store with <postcode>
    When I search for <search_1>
    And Select <product_1>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for click and collect
    And Checkout as guest with <email>
    And Fill in collection details with <first_name>, <last_name> and <mobile_number>
    And Click save and continue
    And Fill in gift card details with <gift_card_number> and <pin>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order for click and collect

    Examples:
      | postcode  | search_1               | product_1                                 | number | email                           | first_name | last_name | mobile_number | gift_card_number   | pin   | address_1        | address_2      | town    |
      | "LS1 6JL" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "3"    | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "9876541321379371" | "339" | "5 Albion Place" | "Albion Court" | "Leeds" |

  @Regression
  Scenario Outline: Go to checkout and use E-voucher and Gift Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in e-voucher details with <e-voucher_number> and <pin>
    Then Check e-voucher applied
    And Add another gift card with <gift_card_number> and <gift_card_pin>


    Examples:
      | search_1         | product_1                                   | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | e-voucher_number   | pin   | gift_card_number   | gift_card_pin |
      | "sewing machine" | "Janome HC8100 Computerised Sewing Machine" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "9876552047173371" | "730" | "9876541322134018" | "140"         |

  @Regression
  Scenario Outline: Go to checkout and use E-voucher
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in e-voucher details with <e-voucher_number> and <pin>
    Then Check e-voucher applied

    Examples:
      | search_1         | product_1                                   | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | e-voucher_number   | pin   |
      | "sewing machine" | "Janome HC8100 Computerised Sewing Machine" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "9876552047173371" | "730" |

  @Smoke @Regression
  Scenario Outline: Add random item to basket and checkout as registered user using Gift Card and Pay by Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in gift card details with <gift_card_number> and <pin>
    Then Check outstanding <balance> for multiple payments
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>

    Examples:
      | search_1         | product_1                                   | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | gift_card_number   | pin   | card_number        | expiry_date | CVC   | name     | balance  |
      | "sewing machine" | "Janome HC8100 Computerised Sewing Machine" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "9876541322134018" | "140" | "4444333322221111" | "03/30"     | "737" | "T Test" | "130.00" |

