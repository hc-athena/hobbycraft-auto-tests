Feature: Purchase Journey

  @Regression
  Scenario Outline: Add random item to basket and checkout as registered user using Pay By Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1               | product_1                                 | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |

  @Regression
  Scenario Outline: Add random item to basket from PDP and checkout as guest using Pay By Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as guest with <email>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1               | product_1                                 | email                           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |

  @Smoke @Regression
  Scenario Outline: Add random item to basket from PLP and checkout as guest using Pay By Card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Add the item to basket from PLP
    And Checkout as guest with <email>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1               | email                           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "White Modelling Clay" | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |


  @Regression
  Scenario Outline: Add random item to basket and checkout as registered user using PayPal
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Select pay by PayPal
    And Login to PayPal with <payPalUsername> and <payPalPassword>
    Then Complete order for PayPal

    Examples:
      | search_1               | product_1                                 | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | payPalUsername          | payPalPassword    |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "calum.brown@ten10.com" | "Hobbycraft2021!" |


  @Regression @Ignore
  Scenario Outline: Add e-voucher to basket and checkout as guest with pay by card
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Fill out e-voucher details with <name>, <recipientName>, <recipientEmail>, <date> and <message>
    And Add the item to basket from PDP for Evoucher
    And Checkout as guest with <email>
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1    | product_1              | email                           | recipientName | recipientEmail                  | date       | message                                        | card_number        | expiry_date | CVC   | name     | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  |
      | "Gift card" | "Hobbycraft e-Voucher" | "olivia.quickenden+1@ten10.com" | "test"        | "olivia.quickenden+1@ten10.com" | "31012022" | "Merry Christmas from the best team in ten10!" | "4444333322221111" | "03/30"     | "737" | "T Test" | "Test"     | "Test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" |

  @Regression
  Scenario Outline: Add digital item to basket and download
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Sign in to download item
    And I sign in to download with <username> and <password>
    And Download item

    Examples:
      | search_1             | product_1                                           | username                        | password           |
      | "Easy Knit Cardigan" | "FREE PATTERN Sirdar Snuggly DK Easy Knit Cardigan" | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" |


