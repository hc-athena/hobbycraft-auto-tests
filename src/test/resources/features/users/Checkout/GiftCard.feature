@Api @Regression @Smoke
Feature: Fetch specific gift card details & balance  Gift card balance check call from SFCC to NAV

  Scenario Outline: Looking up for various kinds of Gift Cards
    When I lookup "<Gift Card Number>" having "<Security Code>"
    Then the gift card  results should be aligned with json schema "GiftCardDetailsSchema"
    And the fetched results should have  "<cardNumber>" along with "<cardType>" "<status>" "<amount>"
    Examples:
      | Gift Card Number | Security Code | cardNumber       | cardType  | status  | amount |
      | 9876540821943385 | MzQ0          | 9876540821943385 | GIFTCARDS | valid   | 17.5   |
      | 9876540123456789 | ODM5          | null             | null      | invalid | null   |
      | 9876551926212162 | MTIw          | 9876551926212162 | GIFTCARDS | blocked | 35     |
      | 9876540601951044 | ODM5          | 9876540601951044 | GIFTCARDS | invalid | 0      |

    #Observations : The Dev Environment Response is not aligned with the Web Experience API response. We were getting the Gift Card Response for "amount" as String
    #rather than Integer . For now I have aligned the schema to give String rather than Integer for it
     #to pass but this needs investigation