Feature: Add to basket without completing order

  @Regression
  Scenario Outline: Add random item to basket from PDP without completing order
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    Then The 'Product added to basket' alert should appear

    Examples:
      | search_1               | product_1                                 |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" |

  @Regression
  Scenario Outline: Add multiple of same item to basket from PDP without completing order
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Increase product quantity with <number>
    And Add increased quantity to basket from PDP
    And Select view basket for home delivery
    Then Check correct <number> of items in basket

    Examples:
      | search_1               | product_1                                 | number |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "5"    |

  @Regression
  Scenario Outline: Add multiple different items to basket from PDP without completing order
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Continue shopping
    When I search for <search_2>
    And Select <product_2>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    Then Check <product_1> and <product_2> are present

    Examples:
      | search_1               | product_1                                 | search_2      | product_2                        |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "Cream roses" | "Cream Wired Rose Heads 20 Pack" |

  @Regression
  Scenario Outline: Add random item to basket then remove it
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Add the item to basket from PLP
    And Remove item from basket
    Then Check product removed successfully

    Examples:
      | search_1               |
      | "White Modelling Clay" |

  @Regression
  Scenario Outline: Add multiple different items to basket then remove one
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Continue shopping
    When I search for <search_2>
    And Select <product_2>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Remove item from basket
    Then Check product removed successfully

    Examples:
      | search_1               | product_1                                 | search_2      | product_2                        |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "Cream roses" | "Cream Wired Rose Heads 20 Pack" |

  @Regression
  Scenario Outline: Add promo code and assert incorrect code
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Add promo code with <promoCode>
    Then Check if promo code is accepted

    Examples:
      | search_1               | product_1                                 | promoCode |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "ten10"   |

  @Smoke @Regression
  Scenario Outline: Add promo code and assert code applied
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Add promo code with <promoCode>
    Then Check promo code applied

    Examples:
      | search_1   | product_1                      | promoCode |
      | "Glue gun" | "Black Mini Hot Melt Glue Gun" | "10off"   |

  @Ignore
  Scenario Outline: Add random item to basket and checkout as registered user using PayPal on PDP page
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Login to PayPal with <payPalUsername> and <payPalPassword> on PDP
#    Then Complete order for PayPal

    Examples:
      | search_1               | product_1                                 | payPalUsername          | payPalPassword    |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "calum.brown@ten10.com" | "Hobbycraft2021!" |
