Feature: Filtering products on PLP

  @Regression
  Scenario Outline: Search product and sort by price low to high with category's
    Given I am on the Hobbycraft home page
    When I click on a <category> then <sub_category>
    Then Check I am on the <sub_category_name> page
    And Sort by price low to high
#    Then Sorting applied - error message appears but only on an auto run - not manually.

    Examples:
      | category     | sub_category         | sub_category_name |
      | "Papercraft" | "Paper & Card Packs" | "Paper & Card"    |


  @Regression @Ignore #test currently not working due to fault on HC site
  Scenario Outline: Search product and sort by price low to high
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Sort by price low to high

  #    Then Sorting applied

    Examples:
      | search_1               |
      | "White Modelling Clay" |


  @Ignore #test currently only works when run through firefox - not chrome
  Scenario Outline: Search product and apply filters
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Apply colour filter
    Then Check filter is successfully applied

    Examples:
      | search_1               |
      | "White Modelling Clay" |