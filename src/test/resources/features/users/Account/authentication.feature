Feature: Authentication journey

  @Regression
  Scenario Outline: Login as a registered user and change the name
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then Check I am signed in as <original_name>
    And To edit <new_name> enter <password>
    Then Check name has been successfully changed to <new_name>
    And To edit <original_name> enter <password>
    Then Check name has been successfully changed to <original_name>

    Examples:
      | username                        | password           | new_name      | original_name |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "Olivia Test" | "Olivia"      |

  @Regression
  Scenario Outline: Login as a registered user and add a card to the account
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Ensure existing cards list is clear
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for add card test
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in and save payment details with <card_number>, <expiry_date>, <CVC>, <name>
    Then Complete order
    Then Check card successfully added to account

    Examples:
      | username                        | password           | search_1               | product_1                                 | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |


  @Smoke @Regression
  Scenario Outline: Login as a registered user and add an address to the account
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Ensure address list is clear
    And Add new address with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Check address saved
    And Delete address
    Then Check address has been deleted

    Examples:
      | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" |


  @Smoke @Regression
  Scenario Outline: Create a new account
    Given I am on the Hobbycraft home page
    When I create an account with <first_name>, <last_name>, <phone_number>, <email_address>, <confirm_email_address>, <password> and <confirm_password>
    Then Check account is successfully created

    Examples:
      | first_name | last_name    | phone_number  | email_address        | confirm_email_address | password           | confirm_password   |
      | "Olivia"   | "Quickenden" | "10987654321" | "olivia.quickenden+" | "olivia.quickenden+"  | "Password123!rtg4" | "Password123!rtg4" |


  @Regression
  Scenario Outline: Forgot password
    Given I am on the Hobbycraft home page
    When I click forgot password with <email>
    Then Check forgot password email pop up appears

    Examples:
      | email                         |
      | "olivia.quickenden@ten10.com" |

  @Regression
  Scenario Outline: Login as a registered user and change the password
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then Check I am signed in as <original_name>
    And Change from <password> to <new_password>
    Then Check password successfully changed
    And Change from <new_password> to <password>

    Examples:
      | username                        | password           | original_name | new_password       |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "Olivia"      | "TestPassword123!" |

  @Regression
  Scenario Outline: Login as a registered user and navigate to the orders page
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then Navigate to the Orders page


    Examples:
      | username                        | password           |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" |

  @Regression
  Scenario Outline: Login as a registered user, make an order and check Order page updated
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in and save payment details with <card_number>, <expiry_date>, <CVC>, <name>
    Then Complete order
    Then Check latest order updated

    Examples:
      | username                        | password           | search_1               | product_1                                 | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |

  @Regression
  Scenario Outline: Login as a registered user and sign out
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then I am able to successfully sign out

    Examples:
      | username                        | password           |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" |

@Smoke @Regression
  Scenario Outline: Login as a registered user and check status of order
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Navigate to the Orders page
    And Find order now with <order_number>
    Then Check order status

    Examples:
      | username                        | password           | order_number |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "00007911"   |

  @Regression
  Scenario Outline: Login as a registered user, make an order and check status of last order
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Checkout as registered user with <username> and <password>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in and save payment details with <card_number>, <expiry_date>, <CVC>, <name>
    Then Complete order
    Then Check latest order status

    Examples:
      | username                        | password           | search_1               | product_1                                 | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |
