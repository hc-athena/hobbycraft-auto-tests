Feature: Browsing through the site

  @Regression
  Scenario: Look at the FAQs and return to home
    Given I am on the Hobbycraft home page
    When Click on the FAQs
    Then Check navigated to FAQs page
    And Click the Hobbycraft logo to return home
    Then Check navigated back to homepage

  @Regression
  Scenario Outline: Navigate to CLP using top level categories
    Given I am on the Hobbycraft home page
    When I click on a <category> then <sub_category>
    Then Check I am on the <sub_category_name> page

    Examples:
      | category     | sub_category         | sub_category_name |
      | "Papercraft" | "Paper & Card Packs" | "Paper & Card"    |

  @Regression
  Scenario Outline: Change country
    Given I am on the Hobbycraft home page
    When I change the country to <country>
    Then The country has been successfully changed to <shippingCountry>

    Examples:
      | country         | shippingCountry |
      | "United States" | "US / USD"      |

  @Regression
  Scenario: Scroll to bottom of homepage and return to top
    Given I am on the Hobbycraft home page
    When I scroll all the way to the bottom
    Then I click the back to top button







#  Scenario Outline: Find workshops near me
#    Given I am on the Hobbycraft home page
#    When I click on the workshops page
#    And I type in my <postcode>
#   # And I choose a craft type
#    And Apply filters
#    And Choose a workshop
#    And Book a slot
#
#    Examples:
#      | postcode  |
#      | "LS1 6JL" |







