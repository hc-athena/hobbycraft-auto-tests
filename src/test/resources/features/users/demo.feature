Feature: Automation Demo


  @Demo
  Scenario Outline: Login as a registered user and check status of order DEMO
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Navigate to the Orders page
    And Find order now with <order_number>
    Then Check order status

    Examples:
      | username                        | password           | order_number |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "00007911"   |

  @Demo
  Scenario Outline: Add random item to basket from PLP and checkout as guest using Pay By Card DEMO
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Add the item to basket from PLP
    And Checkout as guest with <email>
    And Fill in delivery details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    And Fill in payment details with <card_number>, <expiry_date>, <CVC>, <name>
    And Fill in billing details with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Complete order

    Examples:
      | search_1               | email                           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  | card_number        | expiry_date | CVC   | name     |
      | "White Modelling Clay" | "olivia.quickenden+1@ten10.com" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" | "4444333322221111" | "03/30"     | "737" | "T Test" |

  @Demo
  Scenario Outline: Add promo code and assert code applied DEMO
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add the item to basket from PDP
    And Select view basket for home delivery
    And Add promo code with <promoCode>
    Then Check promo code applied

    Examples:
      | search_1   | product_1                      | promoCode |
      | "glue gun" | "Black Mini Hot Melt Glue Gun" | "10off"   |


 @Demo
  Scenario Outline: Login as a registered user and add an address to the account DEMO
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Ensure address list is clear
    And Add new address with <first_name>, <last_name>, <mobile_number>, <address_1>, <address_2>, <town> and <postcode>
    Then Check address saved
    And Delete address
    Then Check address has been deleted

    Examples:
      | username                        | password           | first_name | last_name | mobile_number | address_1        | address_2      | town    | postcode  |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "test"     | "test"    | "12345678910" | "5 Albion Place" | "Albion Court" | "Leeds" | "LS1 6JL" |
