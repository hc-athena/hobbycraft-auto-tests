Feature: Adding products to wishlist


  @Smoke @Regression
  Scenario Outline: Add to wishlist and check wishlist page
    Given I am on the Hobbycraft home page
    When I search for <search_1>
    And Select <product_1>
    And Add product to wishlist
    And Navigate to the wishlist page
    Then Check item is in wishlist page

    Examples:
      | search_1               | product_1                                 |
      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" |

  @Regression
  Scenario Outline: Add multiple items to wishlist and check wishlist page as registered user
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then Check I am signed in as <name>
    And Ensure wishlist is clear
    When I search for <search_1>
    And Select <product_1>
    And Add product to wishlist
    When I search for <search_2>
    And Select <product_2>
    And Add product to wishlist
    And Navigate to the wishlist page
    Then Check <product_1> and <product_2> are present
    And Clear <product_1> and <product_2> from wishlist

    Examples:
      | username                        | password           | name     | search_1               | product_1                                 | search_2      | product_2                        |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "Olivia" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "Cream roses" | "Cream Wired Rose Heads 20 Pack" |


  @Regression
  Scenario Outline: Check wishlist page whilst signed in
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    And Navigate to the wishlist page
    Then Check successfully navigated to wishlist page with <name>

    Examples:
      | username                        | password           | name     |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "Olivia" |

  @Regression
  Scenario: Add recommended to wishlist
    Given I am on the Hobbycraft home page
    When I add to wishlist from recommendations
    Then Check item has been added to wishlist

  @Regression
  Scenario: Add recommended idea to wishlist
    Given I am on the Hobbycraft home page
    When I add to wishlist from idea recommendations
    Then Check item has been added to wishlist

  @Regression
  Scenario Outline: Add to wishlist and delete
    Given I am on the Hobbycraft home page
    When I sign in with <username> and <password>
    Then Check I am signed in as <name>
    And Ensure wishlist is clear
    When I search for <search_1>
    And Select <product_1>
    And Add product to wishlist
    And Navigate to the wishlist page


    Examples:
      | username                        | password           | name     | search_1               | product_1                                 |
      | "olivia.quickenden+1@ten10.com" | "Password123!rtg4" | "Olivia" | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" |


#  Scenario Outline: Add 'you may also like' to wishlist
#    Given I am on the Hobbycraft home page
#    When I search for <search_1>
#    And Select <product_1>
#    When I search for <search_2>
#    And Select <product_2>
#    And Add you may also like item to wishlist
#    Then Check item has been added to wishlist
#
#    Examples:
#      | search_1               | product_1                                 | search_2      | product_2                        |
#      | "White Modelling Clay" | "DAS White Air Drying Modelling Clay 1kg" | "Cream roses" | "Cream Wired Rose Heads 20 Pack" |
