Feature: Journeys around ideas

@Smoke @Regression
  Scenario Outline: I go to ideas and add the ideas to wishlist
    Given I am on the Hobbycraft home page
    When I search for <search_1> idea
    And Select <product_1>
    And Add product to wishlist
#    Then Discover related ideas

    Examples:
      | search_1      | product_1                           |
      | "dog bandana" | "Cricut: How to Make a Dog Bandana" |

  @Regression
  Scenario Outline: I go to ideas and add recommended products to basket
    Given I am on the Hobbycraft home page
    When I search for <search_1> idea
    And Select <product_1>
    And I add idea products to basket
    And Select view basket for home delivery
    Then Check items are in basket

    Examples:
      | search_1      | product_1                           |
      | "dog bandana" | "Cricut: How to Make a Dog Bandana" |




    #  Scenario Outline: I go to ideas and add the products to basket
#    Given I am on the Hobbycraft home page
#    When I search for <search_1> idea
#    And I toggle to ideas
#    And Select <product_1>
#    And View project
#
#    Examples:
#      | search_1                | product_1                       |
#      | "christmas door wreath" | "8 Christmas Door Wreath Ideas" |