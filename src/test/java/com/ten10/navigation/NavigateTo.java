package com.ten10.navigation;

import com.ten10.pages.ActivationPage;
import com.ten10.pages.HomePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebDriver;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class NavigateTo extends PageObject {

    HomePage hobbycraftHomepage;

    ActivationPage activationPage;

    @FindBy(css = "h1")
    private WebElementFacade pageHeader;


    @Step("Open the Hobbycraft home page")
    public void hobbycraftHomePage() {
        hobbycraftHomepage.open();
        getDriver().manage().window().maximize();
    }

    public void activationPage(String activationlink) {
        activationPage.open();
        getDriver().navigate().to(activationlink);
    }


    public void assertPageIsDisplayed(String page) {
        assertThat(pageHeader.getText().equalsIgnoreCase(page), is(true));
    }
}