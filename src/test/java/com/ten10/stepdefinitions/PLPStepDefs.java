package com.ten10.stepdefinitions;

import com.ten10.pages.PDPage;
import com.ten10.pages.PLPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class PLPStepDefs {

    @Steps
    PLPage plPage;

    @Steps
    PDPage pdPage;

    @And("Select {string}")
    public void selectProduct(String product) {
        plPage.selectProduct(product);
    }

    @And("Add the item to basket from PLP")
    public void addProductFromPLP() {
        plPage.addProductFromPLP();
        plPage.clickBasketHeaderLink();
    }

    @And("Select view basket for home delivery PLP")
    public void selectViewBasket(){
        plPage.selectViewBasket();
        }

    @And("Sort by price low to high")
    public void priceLowToHigh() {
        plPage.priceLowToHigh();
    }

    @Then("Sorting applied")
    public void sortingApplied() {
        plPage.sortingApplied();
    }

    @Then("Check I am on the {string} page")
    public void checkCategoryTitle(String sub_category) {
        plPage.checkCategoryTitle(sub_category);

    }

    @And("I toggle to ideas")
    public void toggleIdeas(){
        plPage.toggleIdeas();
    }

}
