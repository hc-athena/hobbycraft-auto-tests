package com.ten10.stepdefinitions;

import com.ten10.pages.BasketPage;
import com.ten10.pages.CheckoutDetailsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CheckoutDetailsStepDefs {

    @Steps
    CheckoutDetailsPage checkoutDetailsPage;

    @Steps
    BasketPage basketPage;

    @And("Fill in delivery details with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void fillInDeliveryDetails(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {
        checkoutDetailsPage.editAddress();
        checkoutDetailsPage.deliveryDetailsFormFill(first_name, last_name, phone_number, address_1, address_2, town, postcode);
    }

    @And("Fill in payment details with {string}, {string}, {string}, {string}")
    public void fillInPaymentDetails(String card_number, String expiry_date, String CVC, String name) {
        checkoutDetailsPage.clickPayByCard();
        checkoutDetailsPage.paymentDetailsFormFill(card_number, expiry_date, CVC, name);
//        checkoutDetailsPage.reviewOrder();
    }

    @And("Fill in and save payment details with {string}, {string}, {string}, {string}")
    public void fillInAndSavePaymentDetails(String card_number, String expiry_date, String CVC, String name) {
        checkoutDetailsPage.clickPayByCard();
        checkoutDetailsPage.paymentDetailsFormFill(card_number, expiry_date, CVC, name);
        checkoutDetailsPage.tickSaveCard();
        checkoutDetailsPage.reviewOrder();
    }

    @And("Checkout as guest with {string}")
    public void checkoutAsGuest(String email) {
        basketPage.selectCheckoutButton();
        checkoutDetailsPage.checkoutAsGuest(email);
    }

    @Then("Select pay by PayPal")
    public void checkPayPalIsSelected() {
        checkoutDetailsPage.payByPal();
        checkoutDetailsPage.checkPayPalIsSelected();
    }

    @And("Login to PayPal with {string} and {string}")
    public void loginToPayPal(String payPalUsername, String payPalPassword) {
        checkoutDetailsPage.selectPayByPayPal();
        checkoutDetailsPage.loginToPayPal(payPalUsername, payPalPassword);
    }

    @And("Login to PayPal with {string} and {string} on PDP")
    public void loginToPayPalOnPDP(String payPalUsername, String payPalPassword) {
        checkoutDetailsPage.selectPayByPayPalOnPDP();
        checkoutDetailsPage.loginToPayPal(payPalUsername, payPalPassword);
    }

    @And("Fill in gift card details with {string} and {string}")
    public void enterGiftCardNumber(String giftCardNumber, String pin) {
        checkoutDetailsPage.clickUseGiftCard();
        checkoutDetailsPage.enterGiftCardNumber(giftCardNumber, pin);
//        checkoutDetailsPage.checkGiftCard();
    }

    @Then("Check e-voucher applied")
    public void checkGiftCardDetails(){
        checkoutDetailsPage.giftCardDetails();
    }

    @And("Fill in e-voucher details with {string} and {string}")
    public void enterEVoucherNumber(String eVoucherNumber, String pin) {
        checkoutDetailsPage.clickUseGiftCard();
        checkoutDetailsPage.enterGiftCardNumber(eVoucherNumber, pin);
    }

    @And("Add another gift card with {string} and {string}")
    public void addAnotherGiftCard(String giftCardNumber, String pin) {
        checkoutDetailsPage.addAnotherGiftCard(giftCardNumber, pin);
    }

    @And("Fill in collection details with {string}, {string} and {string}")
    public void collectionDetails(String first_name, String last_name, String phone_number) {
        checkoutDetailsPage.collectionDetails(first_name, last_name, phone_number);
    }

    @And("Fill in billing details with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void billingDetails(String first_name, String last_name, String mobile_number, String address_1, String address_2, String town, String postcode) {
        checkoutDetailsPage.changeBillingAddress();
        checkoutDetailsPage.billingDetailsFormFill(first_name, last_name, mobile_number, address_1, address_2, town, postcode);
    }

    @And("Click save and continue")
    public void saveAndContinue() {
        checkoutDetailsPage.saveAndContinue();
    }

    @Then("Complete order")
    public void completeOrder() {
        checkoutDetailsPage.completeOrder();
        checkoutDetailsPage.confirmationOfOrder();
    }

    @Then("Complete order for click and collect")
    public void completeOrderForClickAndCollect() {
        checkoutDetailsPage.completeOrder();
        checkoutDetailsPage.confirmationOfOrderForClickAndCollect();
    }

    @Then("Complete order for PayPal")
    public void completeOrderForPayPal() {
        checkoutDetailsPage.completeOrderForPayPal();
        checkoutDetailsPage.confirmationOfOrder();
    }

    @Then("Complete order for PayPal for click and collect")
    public void completeOrderForPayPalForClickAndCollect() {
        checkoutDetailsPage.completeOrderForPayPal();
        checkoutDetailsPage.confirmationOfOrderForClickAndCollect();
    }

    @Then("Check outstanding {string} for multiple payments")
    public void checkMultiplePayments(String balance) {
        checkoutDetailsPage.checkOutstandingBalance(balance);

    }


}

