package com.ten10.stepdefinitions;

import com.ten10.pages.BasketPage;
import com.ten10.pages.PDPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en_scouse.An;
import net.thucydides.core.annotations.Steps;

public class PDPStepDefs {

    @Steps
    PDPage pDPage;

    @Steps
    BasketPage basketPage;

    @And("Add the item to basket from PDP")
    public void addItemToBasket() {
        pDPage.addToBasket();
    }

    @And("Add the item to basket from PDP for Evoucher")
    public void addItemToBasketEvoucher() {
        pDPage.addToBasketEvoucher();
        pDPage.selectViewBasketButton();
    }


    @And("Add increased quantity to basket from PDP")
    public void addIncreasedQuantityToBasket() {
        pDPage.addToBasket();
    }

    @Then("The 'Product added to basket' alert should appear")
    public void checkItemAddedToBasket() {
        pDPage.checkProductAddedToBasket();
    }

    @And("Select view basket for home delivery")
    public void selectViewBasket() {
        pDPage.selectViewBasketButton();
        basketPage.selectHomeDelivery();
    }

    @And("Select view basket for add card test")
    public void selectViewBasketForAddCard() {
        pDPage.selectViewBasketButton();
        basketPage.selectHomeDeliveryAddCard();
        basketPage.selectCheckoutButton();
    }

    @And("Select view basket for click and collect")
    public void selectViewBasketForClickAndCollect() {
        pDPage.selectViewBasketButton();
        basketPage.selectClickAndCollect();
    }

    @And("Apply colour filter")
    public void applyColourFilter() {
        pDPage.clickColourFilter();
    }

    @Then("Check filter is successfully applied")
    public void checkFilterIsApplied() {
        pDPage.checkFilterIsApplied();
    }

    @And("Add you may also like item to wishlist")
    public void addItemToWishlist() {
        pDPage.addToWishlist();
    }

    @Then("Check item has been added to wishlist")
    public void checkItemIsAddedToWishlist() {
        pDPage.addedToWishlistAlert();
    }

    @And("Sign in to download item")
    public void signInToDownloadItem(){
        pDPage.signInToDownload();
    }

    @And("Download item")
    public void clickDownload(){
        pDPage.clickDownloadItem();
    }


    @And("Increase product quantity with {string}")
    public void increaseProductQuantity(String number) {
        pDPage.increaseQuantity(number);
        pDPage.checkIncreaseQuantity();
    }

    @And("Check that quantity has increased")
    public void checkQuantityIncreased(){
        pDPage.checkIncreaseQuantity();
    }

    @And("Add product to wishlist")
    public void addToWishlistButton(){
        pDPage.addToWishlistButton();
        pDPage.addedToWishlistAlert();
    }

    @And("Navigate to the wishlist page")
    public void clickOnWishlistPage(){
        pDPage.clickOnWishlistPage();
    }

    @Then("Check item is in wishlist page")
    public void checkItemIsInWishlistPage(){
        pDPage.checkItemInWishlistPage();
    }

    @And("Fill out e-voucher details with {string}, {string}, {string}, {string} and {string}")
    public void evoucherDetails(String name, String recipientName, String recipientEmail, String date, String message){
        pDPage.evoucherDetails(name, recipientName, recipientEmail, date, message);
    }

    @Then("Check correct {string} of items in basket")
    public void checkNumberItemsInBasket(String number){
        pDPage.checkNumberItemsInBasket(number);
    }

    @And("Select store with {string}")
    public void selectStore(String postcode){
        pDPage.selectStore(postcode);
    }


    @And("View project")
    public void viewProject(){
        pDPage.viewProject();
    }

    @Then("Discover related ideas")
    public void discoverIdeas(){
        pDPage.discoverIdeas();
    }

    @And("I add idea products to basket")
    public void addIdeaProducts(){
        pDPage.addIdeaProducts();
    }

}
