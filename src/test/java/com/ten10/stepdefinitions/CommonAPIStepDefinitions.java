package com.ten10.stepdefinitions;

import io.cucumber.java.en.Then;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.then;

public class CommonAPIStepDefinitions {
    @Then("the results should be aligned with json schema {string}")
    public void theResultsShouldBeAlignedWithJsonSchema(String jsonSchema) {
        then().body(matchesJsonSchemaInClasspath("jsonschema/"+ jsonSchema +".json"));
    }
}
