package com.ten10.stepdefinitions;

import com.ten10.pages.HomePage;
import com.ten10.pages.StoresPage;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

public class StoresStepDefs {

    @Steps
    HomePage homePage;

    @Steps
    StoresPage storesPage;


    @And("Choose my preferred store with {string}")
    public void storeSearchBar(String postcode){
        homePage.clickStores();
        storesPage.storeSearchBar(postcode);
        storesPage.makeMyPreferredStore();
    }

}
