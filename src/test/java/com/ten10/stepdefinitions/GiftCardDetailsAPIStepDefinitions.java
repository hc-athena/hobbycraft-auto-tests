package com.ten10.stepdefinitions;

import com.ten10.apiServices.GiftCardAPI;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.*;

public class GiftCardDetailsAPIStepDefinitions {

    @Steps
    GiftCardAPI giftCardAPI;

    @When("I lookup {string} having {string}")
    public void iLookupHaving(String giftCardNumber, String securityCode) {
        giftCardAPI.fetchGiftCardDetails(giftCardNumber,securityCode) ;
    }

    @Then("the fetched results should have  {string} along with {string} {string} {string}")
    public void theFetchedResultsShouldHaveAlongWith(String cardNumber, String cardType, String status, String amount) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("cardNumber", anyOf(nullValue(),equalTo(cardNumber))));
    }


    @Then("the gift card  results should be aligned with json schema {string}")
    public void theGiftCardResultsShouldBeAlignedWithJsonSchema(String arg0) {
        then().body(matchesJsonSchemaInClasspath("jsonschema/"+ arg0 +".json"));
    }
}
