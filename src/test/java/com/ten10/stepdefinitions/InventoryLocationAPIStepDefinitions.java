package com.ten10.stepdefinitions;

import com.ten10.apiServices.InventoryLocationAPI;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.is;

public class InventoryLocationAPIStepDefinitions {
    @Steps
    InventoryLocationAPI inventoryLocationAPI;

    @When("I lookup {string} in  {string}")
    public void iLookupIn(String productSKUS, String locations) {
        inventoryLocationAPI.fetchInventoryLocationAvailDetails(productSKUS,locations);
    }

    @And("the fetched results should have  {string} along with {string}")
    public void theFetchedResultsShouldHaveAlongWith(String validSKU, String validLocation) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("[0].validSKU.", is(Boolean.valueOf(validSKU))));
        restAssuredThat(response -> response.body("[0].availabilities.validLocation[0]", is(Boolean.valueOf(validLocation))));
    }
}
