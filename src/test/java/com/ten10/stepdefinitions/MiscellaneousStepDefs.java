package com.ten10.stepdefinitions;

import com.ten10.pages.MiscellaneousPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class MiscellaneousStepDefs {

    @Steps
    MiscellaneousPage miscellaneousPage;

    @Then("Check navigated to FAQs page")
    public void checkOnFAQsPage(){
        miscellaneousPage.checkFAQsPage();
    }


    @And("Click the Hobbycraft logo to return home")
    public void goToHomepage(){
        miscellaneousPage.goToHomepage();
    }


}
