package com.ten10.stepdefinitions;

import com.ten10.pages.BasketPage;
import com.ten10.pages.LoginPage;
import com.ten10.pages.PDPage;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class BasketPageStepDefs {

    @Steps
    BasketPage basketPage;

    @Steps
    LoginPage loginPage;

    @And("Checkout as registered user with {string} and {string}")
    public void selectCheckout(String username, String password) {
        basketPage.selectCheckoutButton();
        loginPage.login(username, password);
        basketPage.selectSignInAndCheckoutButton();
    }

    @And("Click find a store")
    public void clickFindAStore() {
        basketPage.clickFindAStore();
    }

    @And("Check minimum spend alert appears")
    public void checkSpendAlert() {
        basketPage.checkSpendAlert();
    }

    @And("Add promo code with {string}")
    public void addPromoCode(String promoCode) {
        basketPage.addPromoCode(promoCode);
    }

    @Then("Check promo code applied")
    public void checkPromoApplied(){
        basketPage.checkPromoApplied();
    }

    @Then("Check if promo code is accepted")
    public void checkPromoCode(){
        basketPage.checkPromoCodeErrorMessage();
    }

    @Then("Check {string} and {string} are present")
    public void checkCorrectItemsInBasket(String product_1, String product_2) {
        basketPage.checkCorrectItemsInBasket(product_1, product_2);
    }

    @And("Remove item from basket")
    public void removeItem() {
        basketPage.removeItemFromBasket();
    }

    @Then("Check product removed successfully")
    public void productSuccessfullyRemoved() {
        basketPage.productRemovedAlert();
    }

    @And("Continue shopping")
    public void continueShopping() {
        basketPage.continueShopping();
    }

    @Then("Check items are in basket")
    public void checkItemsInBasket() {
        basketPage.itemsInBasket();
    }


}
