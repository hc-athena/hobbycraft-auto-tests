package com.ten10.stepdefinitions;

import com.ten10.navigation.NavigateTo;
import com.ten10.pages.HomePage;
import com.ten10.pages.MiscellaneousPage;
import com.ten10.pages.PDPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class HomeStepDefs {

    @Steps
    NavigateTo navigateTo;

    @Steps
    HomePage homePage;

    @Steps
    MiscellaneousPage miscellaneousPage;

    @Steps
    PDPage pdPage;


    @Given("I am on the Hobbycraft home page")
    public void i_am_on_the_hobbycraft_home_page() {
        navigateTo.hobbycraftHomePage();
        homePage.clickAcceptCookies();
    }

    @Then("the {string} page is displayed")
    public void thePageIsDisplayed(String page) throws Throwable {
        navigateTo.assertPageIsDisplayed(page);
    }

    @When("I search for {string}")
    public void clickSearchBarButton(String product) {
        miscellaneousPage.scrollToHomepage();
        homePage.clickSearchBarButton();
        homePage.searchForProduct(product);
        homePage.enterSearch();
    }

    @When("I search for {string} idea")
    public void clickSearch(String product) {
        miscellaneousPage.scrollToHomepage();
        homePage.clickSearchBarButton();
        homePage.searchForProduct(product);
        homePage.viewAllIdeas();
    }

    @And("Search for {string}")
    public void searchForProduct(String product) {
        homePage.searchForProduct(product);
        homePage.enterSearch();
    }

    @And("I click on the workshops page")
    public void clickWorkshops() {
        homePage.clickWorkshops();
    }

    @And("Click on the FAQs")
    public void clickOnFAQs() {
        homePage.clickOnFAQs();
    }

    @Then("Check navigated back to homepage")
    public void checkOnHomePage() {
        homePage.checkOnHomePage();
    }

    @When("I add to wishlist from recommendations")
    public void wishlistFromRecommended() {
        homePage.wishlistRecommended();
    }

    @When("I add to wishlist from idea recommendations")
    public void wishListFromIdeas() {
        homePage.wishListFromIdea();
    }

    @When("I click on a {string} then {string}")
    public void chooseCategory(String category, String sub_category) {
        homePage.chooseCategory(category, sub_category);
    }

    @When("I change the country to {string}")
    public void changeCountry(String country){
        homePage.selectCountry(country);
    }

    @Then("The country has been successfully changed to {string}")
    public void checkCountry(String country){
        homePage.checkCountry(country);
    }

    @When("I scroll all the way to the bottom")
    public void scrollToBottom(){
        homePage.scrollToBottom();
    }

    @Then("I click the back to top button")
    public void clickBackToTop(){
        homePage.clickBackToTop();
    }

    @When("I click on a recommended idea")
    public void clickRecommendedIdea(){
        homePage.clickRecommendedIdea();
    }










//    @And("I hover and click")
//    public void hoverAndClick(){
//        homePage.hoverAndClick();
//    }

}

