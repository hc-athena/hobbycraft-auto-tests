package com.ten10.stepdefinitions;

import com.ten10.pages.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class AccountStepDefs {

    @Steps
    AccountPage accountPage;

    @Steps
    HomePage homePage;

    @Steps
    PLPage plPage;

    @Steps
    PDPage pDPage;

    @Then("Check I am signed in as {string}")
    public void checkSuccessfulSignIn(String original_name) {
        accountPage.checkSuccessfulSignIn(original_name);
    }

    @Then("Check name has been successfully changed to {string}")
    public void checkNameHasChanged(String name) {
        accountPage.checkNameChanged(name);
    }

    @And("To edit {string} enter {string}")
    public void editNameChange(String original_name, String password) {
        accountPage.selectProfileDetails();
        accountPage.editName(original_name);
        accountPage.enterPassword(password);
        accountPage.selectSaveChangesButton();
    }

    @And("Ensure existing cards list is clear")
    public void clearExistingCards() {
        accountPage.selectPaymentMethods();
        accountPage.deleteExistingCards();
    }

    @Then("Check card successfully added to account")
    public void checkCardAdded() {
        homePage.clickSignInLink();
        accountPage.selectPaymentMethods();
        accountPage.checkCardAdded();
    }

    @And("Add new address with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void addNewAddress(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {
        accountPage.clickAddNewAddress();
        accountPage.addNewAddress(first_name, last_name, phone_number, address_1, address_2, town, postcode);
    }

    @Then("Check address saved")
    public void checkAddressSaved() {
        accountPage.checkAddressSaved();
    }

    @And("Delete address")
    public void deleteAddress() {
        accountPage.deleteAddress();
    }

    @Then("Check address has been deleted")
    public void checkAddressDeleted() {
        accountPage.confirmDeletedAddress();
    }

    @Then("Check successfully navigated to wishlist page with {string}")
    public void wishlistPage(String wishListName) {
        accountPage.wishlistPageName(wishListName);
    }

    @And("Clear {string} and {string} from wishlist")
    public void clearWishlistPage(String product_1, String product_2) {
        plPage.selectProduct(product_1);
        pDPage.addToWishlistButton();
        pDPage.removedFromWishlistAlert();
        pDPage.clickOnWishlistPage();
        plPage.selectProduct(product_2);
        pDPage.addToWishlistButton();
        pDPage.removedFromWishlistAlert();
    }

    @And("Ensure wishlist is clear")
    public void clearWishlist() {
        accountPage.firstWishListItem();
    }

    @And("Change from {string} to {string}")
    public void changePassword(String currentPassword, String newPassword) {
        accountPage.changePassword(currentPassword, newPassword);
    }

    @Then("Check password successfully changed")
    public void checkPasswordChange() {
        accountPage.checkPasswordChanged();
    }

    @And("Ensure address list is clear")
    public void clearAddressList() {
        accountPage.clickAddressBook();
        accountPage.clearAllAddress();
    }

    @Then("Navigate to the Orders page")
    public void checkNavigateToOrders() {
        accountPage.clickOrders();
        accountPage.checkOnOrderPage();
    }

    @Then("Check latest order updated")
    public void checkLatestOrderUpdated() {
        accountPage.checkLatestOrderUpdated();
    }

    @Then("Check latest order status")
    public void checkLatestOrderStatus() {
        accountPage.checkLatestOrderStatus();
    }

    @Then("I am able to successfully sign out")
    public void signOut() {
        accountPage.signOut();
    }

    @And("Find order now with {string}")
    public void findItNow(String order_number) {
        accountPage.findItNow(order_number);
    }

    @Then("Check order status")
    public void checkOrderStatus() {
        accountPage.checkOrderStatus();
    }

}

