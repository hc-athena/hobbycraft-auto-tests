package com.ten10.apiServices;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

public class InventoryLocationAPI {

    private static EnvironmentVariables environmentVariables;


    private static final String INVENTORY_AVAIL_URI = "/availabilities";


    public void fetchInventoryLocationAvailDetails(String productSKUS, String locations) {
        SerenityRest.given().log().all()
                .baseUri(EnvironmentSpecificConfiguration.from(environmentVariables)
                        .getProperty("api.base.url"))
                .relaxedHTTPSValidation()
                .header("Authorization", "Basic NGQ4ZDMyMWI4YjRiNGQ0YWE2MTYxYmE1MzgyZTlhOTI6NzAzMDhGNkRFODExNDFEM0FkMjk0NTNEYmJkN0ZDNzQ=")
                .with()
                .queryParam("productSKUs",productSKUS)
                .queryParam("locations",locations)
                .get(INVENTORY_AVAIL_URI).then().log().all();
    }

}
