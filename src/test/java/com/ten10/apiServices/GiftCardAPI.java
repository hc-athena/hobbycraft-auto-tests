package com.ten10.apiServices;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

public class GiftCardAPI {

    private static EnvironmentVariables environmentVariables;


    private static final String GIFT_CARD_URI = "/gift-cards/{giftcardnumber}";


    public void fetchGiftCardDetails(String giftcardnumber, String securityCode) {
        SerenityRest.given().log().all()
                .baseUri(EnvironmentSpecificConfiguration.from(environmentVariables)
                        .getProperty("api.base.url"))
                .relaxedHTTPSValidation()
                .pathParam("giftcardnumber", giftcardnumber)
                .header("securityCode", securityCode)
                .header("Authorization", "Basic NGQ4ZDMyMWI4YjRiNGQ0YWE2MTYxYmE1MzgyZTlhOTI6NzAzMDhGNkRFODExNDFEM0FkMjk0NTNEYmJkN0ZDNzQ=")
                .get(GIFT_CARD_URI).then().log().all();
    }

}
