package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Set;

public class MiscellaneousPage extends PageObject {


    @FindBy(css = "#maincontent > div.l-static_page > div > main > div > h1")
    private WebElementFacade FAQsPage;

    public void checkFAQsPage() {
        String alert = FAQsPage.getText();
        Assert.assertEquals("FAQs", alert);
    }

    @FindBy(css = ".b-logo_sticky")
    private WebElementFacade goToHomepage;

    public void goToHomepage() {
        goToHomepage.click();
    }

    public void scrollToHomepage() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, 0)");
    }

}


//Useful Code:

// Assert:
//        String alert = webElementFacadeName.getText();
//        Assert.assertEquals("Expected Text", alert);

// Our Pal Javascript:
//    JavascriptExecutor executor = (JavascriptExecutor) getDriver();
//        executor.executeScript("arguments[0].click();", webElementFacadeName);
//        executor.executeScript("arguments[0].scrollIntoView();", webElementFacadeName);

// Wait:
//    waitForCondition().until(ExpectedConditions.visibilityOf(reviewOrderButton));
//       Explicit wait: for 30 seconds
//        WebDriverWait wait = new WebDriverWait(getDriver(),30);
//        wait.until(ExpectedConditions.visibilityOf(giftCardUsed));