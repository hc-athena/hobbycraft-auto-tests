package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class StoresPage extends PageObject {

    @FindBy(id = "dwfrm_storeLocatorSearch_searchByPlace")
    private WebElementFacade storeSearchBar;

    @FindBy(xpath = "//*[@id=\"page-body\"]/ul/li[1]")
    private WebElementFacade firstResult;

    @FindBy(xpath = "//*[@id=\"page-body\"]/ul/li[3]")
    private WebElementFacade thirdResult;

    @FindBy(linkText = "View All Stores")
    private WebElementFacade viewAllStores;

    public void storeSearchBar(String postcode) {
        storeSearchBar.sendKeys(postcode);
        storeSearchBar.sendKeys(Keys.ENTER);
    }

    @FindBy(className = "b-storelocator_result-preferred_btn")
    private WebElementFacade makeMyPreferredStore;

    public void makeMyPreferredStore() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", makeMyPreferredStore);
    }

}
