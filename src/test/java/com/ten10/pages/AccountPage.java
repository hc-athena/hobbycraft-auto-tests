package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class AccountPage extends PageObject {

    @FindBy(className = "b-user_greeting-message")
    private WebElementFacade userGreetingMessage;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/section[1]/div[2]/ul/li[1]")
    private WebElementFacade profileDetailsName;

    public void checkSuccessfulSignIn(String original_name) {
        String alert = userGreetingMessage.getText();
        Assert.assertEquals("Hi, " + original_name, alert);
    }

    public void checkNameChanged(String name) {
        String newAlert = profileDetailsName.getText();
        Assert.assertEquals("Name: " + name + " Liv", newAlert);
    }

    @FindBy(linkText = "Profile Details")
    private WebElementFacade profileDetails;

    public void selectProfileDetails() {
        profileDetails.click();
    }

    @FindBy(id = "dwfrm_profile_customer_firstname")
    private WebElementFacade firstNameBox;

    public void editName(String original_name) {
        firstNameBox.clear();
        firstNameBox.sendKeys(original_name);
    }

    @FindBy(id = "dwfrm_profile_login_currentpassword")
    private WebElementFacade passwordBox;

    public void enterPassword(String password) {
        passwordBox.sendKeys(password);
    }

    @FindBy(xpath = "//*[@id=\"dwfrm_profile\"]/div[9]/button")
    private WebElementFacade saveChangesButton;

    public void selectSaveChangesButton() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", saveChangesButton);
    }

    @FindBy(linkText = "Payment Methods")
    private WebElementFacade clickPaymentMethods;

    public void selectPaymentMethods() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickPaymentMethods);
    }

    @FindBy(css = ".b-cards_grid-delete")
    private WebElementFacade deletePaymentCard;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div[3]/div/div/div[3]/button[1]")
    private WebElementFacade confirmCardDelete;

    @FindBy(css = ".b-account-empty_state")
    private WebElementFacade cardListEmpty;

    public void deleteExistingCards() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        try {
            for (int i = 1; i < (100); i++) {
                executor.executeScript("arguments[0].click();", deletePaymentCard);
                executor.executeScript("arguments[0].click();", confirmCardDelete);
            }
        } catch (Exception e) {
            String noCardsAlert = cardListEmpty.getText();
            Assert.assertEquals("You currently have no saved payment methods", noCardsAlert);
        }
    }

    public void checkCardAdded() {
        String deleteCardButton = deletePaymentCard.getText();
        Assert.assertEquals("Delete Card", deleteCardButton);
    }

    @FindBy(linkText = "Address Book")
    private WebElementFacade clickAddressBook;

    @FindBy(linkText = "+ Add A New Address")
    private WebElementFacade addNewAddressLink;

    public void clickAddressBook() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickAddressBook);
    }

    public void clickAddNewAddress() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addNewAddressLink);
    }

    @FindBy(id = "dwfrm_address_firstName")
    private WebElementFacade firstName;

    @FindBy(id = "dwfrm_address_lastName")
    private WebElementFacade lastName;

    @FindBy(id = "dwfrm_address_phone")
    private WebElementFacade phoneNumber;

    @FindBy(id = "dwfrm_address_address1")
    private WebElementFacade address1;

    @FindBy(id = "dwfrm_address_address2")
    private WebElementFacade address2;

    @FindBy(id = "dwfrm_address_city")
    private WebElementFacade townBox;

    @FindBy(id = "dwfrm_address_postalCode")
    private WebElementFacade postcodeBox;

    @FindBy(name = "dwfrm_address_apply")
    private WebElementFacade addNewAddress;

    public void addNewAddress(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstName.sendKeys(first_name);
        lastName.sendKeys(last_name);
        phoneNumber.sendKeys(String.valueOf(phone_number));
        address1.sendKeys(address_1);
        address2.sendKeys(address_2);
        townBox.sendKeys(town);
        postcodeBox.sendKeys(postcode);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addNewAddress);
    }

    @FindBy(className = "b-cards_grid-delete")
    private WebElementFacade deleteAddress;

    public void checkAddressSaved() {
        String newAlert = deleteAddress.getText();
        Assert.assertEquals("Delete", newAlert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div[3]/div/div/div[3]/button[1]")
    private WebElementFacade confirmDelete;

    public void deleteAddress() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", deleteAddress);
        executor.executeScript("arguments[0].click();", confirmDelete);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade deletedAddressConfirmed;


    public void confirmDeletedAddress() {
        String newAlert = deletedAddressConfirmed.getText();
        Assert.assertEquals("Your address has been deleted. You have no saved addresses", newAlert);
    }

    @FindBy(className = "b-account-empty_state")
    private WebElementFacade emptyAddressBook;

    @FindBy(className = "b-account-title")
    private WebElementFacade addressBookTitle;

    public void clearAllAddress() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        try {
            for (int i = 1; i < (100); i++) {
                executor.executeScript("arguments[0].scrollIntoView();", addressBookTitle);
                executor.executeScript("arguments[0].click();", deleteAddress);
                executor.executeScript("arguments[0].click();", confirmDelete);
            }
        } catch (Exception e) {
            executor.executeScript("arguments[0].scrollIntoView();", emptyAddressBook);
            String newAlert = emptyAddressBook.getText();
            Assert.assertEquals("You currently have no saved addresses", newAlert);
        }
    }


    @FindBy(className = "b-wishlist-title")
    private WebElementFacade wishlistPageName;

    public void wishlistPageName(String wishlistName) {
        String newAlert = wishlistPageName.getText();
        Assert.assertEquals(wishlistName + "'s Wish List", newAlert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div/div[2]/div/div/div/section/div[2]/div[1]/p/a")
    private WebElementFacade firstWishListItem;

    @FindBy(className = "b-product_wishlist-btn")
    private WebElementFacade removeWishlistButton;

    @FindBy(className = "b-header_wishlist-link")
    private WebElementFacade clickOnWishlistPage;

    @FindBy(className = "b-wishlist-empty_text")
    private WebElementFacade wishListEmpty;

    public void firstWishListItem() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        clickOnWishlistPage.click();
        try {
            for (int i = 1; i < (100); i++) {
                executor.executeScript("arguments[0].click();", firstWishListItem);
                executor.executeScript("arguments[0].click();", removeWishlistButton);
                clickOnWishlistPage.click();
            }
        } catch (Exception e) {
            String newAlert = wishListEmpty.getText();
            Assert.assertEquals("You currently have no items", newAlert);
        }
    }

    @FindBy(linkText = "Change Password")
    private WebElementFacade changePasswordLink;

    @FindBy(id = "dwfrm_changePassword_currentpassword")
    private WebElementFacade currentPasswordBox;

    @FindBy(id = "dwfrm_changePassword_newpassword")
    private WebElementFacade newPasswordBox;

    @FindBy(id = "dwfrm_changePassword_newpasswordconfirm")
    private WebElementFacade confirmNewPasswordBox;

    @FindBy(className = "b-form-btn_save")
    private WebElementFacade changePasswordBox;

    public void changePassword(String currentPassword, String newPassword) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", changePasswordLink);
        currentPasswordBox.sendKeys(currentPassword);
        newPasswordBox.sendKeys(newPassword);
        confirmNewPasswordBox.sendKeys(newPassword);
        executor.executeScript("arguments[0].click();", changePasswordBox);
    }

    @FindBy(xpath = " //*[@id=\"maincontent\"]/div[2]/div/div")
    private WebElementFacade checkPasswordChanged;

    public void checkPasswordChanged() {
        String newAlert = checkPasswordChanged.getText();
        Assert.assertEquals("Your password has been successfully changed", newAlert);
    }

    @FindBy(linkText = "Orders")
    private WebElementFacade ordersLink;

    public void clickOrders() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", ordersLink);
    }

    @FindBy(css = ".b-history_page-title")
    private WebElementFacade pageTitle;

    public void checkOnOrderPage() {
        String orderTitle = pageTitle.getText();
        Assert.assertEquals("Orders", orderTitle);
    }

    @FindBy(css = ".b-confirmation_header-value")
    private WebElementFacade confirmationPageOrderNumber;

    @FindBy(css = ".b-header_login-caption")
    private WebElementFacade signInLink;

    @FindBy(css = ".b-order_details-caption")
    private WebElementFacade orderDetailsTitle;

    public void checkLatestOrderUpdated() {
        String orderNumber = confirmationPageOrderNumber.getText();
        signInLink.click();
        WebElement latestOrderNumber = getDriver().findElement(By.linkText(orderNumber));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", latestOrderNumber);
        String orderDetailsHeading = orderDetailsTitle.getText();
        Assert.assertEquals("Order Details", orderDetailsHeading);
    }

    @FindBy(linkText = "View Order History")
    private WebElementFacade viewOrderHistoryLink;

    public void selectViewOrderHistory() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", viewOrderHistoryLink);
    }

    @FindBy(linkText = "Sign out")
    private WebElementFacade signOutButton;

    public void signOut() {
        signOutButton.click();
        String signInHeaderButton = signInLink.getText();
        Assert.assertEquals("Sign in", signInHeaderButton);
    }

    @FindBy(linkText = "Find it now")
    private WebElementFacade findItNowLink;

    @FindBy(id = "dwfrm_trackOrder_orderNumber")
    private WebElementFacade trackerNumber;

    @FindBy(css = ".b-track_order-btn")
    private WebElementFacade trackOrderButton;

    public void findItNow(String order_number) {
        findItNowLink.click();
        trackerNumber.sendKeys(order_number);
        trackOrderButton.click();
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div/ul/li[2]")
    private WebElementFacade orderStatus;

    public void checkOrderStatus() {
        String orderStatusText = orderStatus.getText();
        Assert.assertEquals("Status: Processing", orderStatusText);
    }


    public void checkLatestOrderStatus() {
        String orderNumber = confirmationPageOrderNumber.getText();
        signInLink.click();
        WebElement latestOrderNumber = getDriver().findElement(By.linkText(orderNumber));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", latestOrderNumber);
        String orderStatusText = orderStatus.getText();
        Assert.assertEquals("Status: Order placed", orderStatusText);
    }


}

