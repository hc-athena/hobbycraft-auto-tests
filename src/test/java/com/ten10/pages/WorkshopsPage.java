package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class WorkshopsPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"main-input-region\"]")
    private WebElementFacade typeInPostcode;

    public void typeInPostcode(String postcode) {
        typeInPostcode.sendKeys(postcode);
    }

    @FindBy(xpath = "//*[@id=\"hc-filter-row\"]/div[2]/div[1]/div/div")
    private WebElementFacade chooseCraftType;

    @FindBy(className = "select-box__current")
    private WebElementFacade chooseChristmas;

    public void chooseCraftType() {
        chooseCraftType.click();
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", chooseCraftType);
        executor.executeScript("arguments[0].click();", chooseChristmas);
    }

    @FindBy(id = "hc-apply-filter-btn")
    private WebElementFacade applyFilters;

    public void applyFilters() {
        applyFilters.click();
    }

    @FindBy(xpath = "//*[@id=\"display-classes-wrapper\"]/div/div/a[4]")
    private WebElementFacade chooseWorkshop;

    public void chooseWorkshop() {
        waitForCondition().until(
                ExpectedConditions.visibilityOf(chooseWorkshop));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", chooseWorkshop);
    }

    @FindBy(xpath = "//*[@id=\"main-booking-form\"]/div[2]/button")
    private WebElementFacade clickBook;

    @FindBy(xpath = "//*[@id=\"main-time-selector\"]")
    private WebElementFacade chooseTime;

    public void chooseASlot() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", clickBook);
        executor.executeScript("arguments[0].click();", chooseTime);
        Select dropdown = new Select(getDriver().findElement(By.xpath("//*[@id=\"main-time-selector\"]")));
        dropdown.selectByVisibleText("Thu 23 Dec, 10:30am-11:30am");
        clickBook.click();
    }

}
