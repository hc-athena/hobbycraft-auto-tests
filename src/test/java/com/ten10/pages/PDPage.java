package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.bcel.generic.L2D;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class PDPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/main/section[1]/div[1]/section[2]/div[4]/div[4]/div[1]/button/span[2]")
    private WebElementFacade addToBasket;

    public void addToBasket() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addToBasket);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/main/section/div[1]/section[2]/div[2]/div[5]/div[2]/div[1]/button")
    private WebElementFacade addToBasketEvoucher;

    public void addToBasketEvoucher() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addToBasketEvoucher);
    }

    public void checkProductAddedToBasket() {
        String alert = viewBasketButton.getText();
        Assert.assertEquals("View Basket", alert);
    }

    @FindBy(linkText = "View Basket")
    private WebElementFacade viewBasketButton;

    public void selectViewBasketButton() {
        waitForCondition().until(ExpectedConditions.visibilityOf(viewBasketButton));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", viewBasketButton);
    }

    @FindBy(css = "div[aria-label=White]")
    private WebElementFacade selectColourFilter;

    @FindBy(id = "//*[@id=\"refinements-panel-title-xl\"]")
    private WebElementFacade panel;

    public void clickColourFilter() {
        waitForCondition().until(ExpectedConditions.elementToBeClickable(selectColourFilter));
        getDriver().findElement(By.xpath("//*[@id='refinements-panel-xl']/div/section[3]/div/div/ul/li[position()=2]/div/span[position()=2]")).click();
    }

    @FindBy(className = "b-applied_filters-value")
    private WebElementFacade checkFilterIsApplied;

    public void checkFilterIsApplied() {
        String filterName = checkFilterIsApplied.getText();
        Assert.assertEquals("White", filterName);
    }

    @FindBy(css = ".b-wishlist-icon")
    private WebElementFacade addToWishlist;

    public void addToWishlist() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", addToWishlist);
        addToWishlist.click();
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade alteredWishlistAlert;

    public void addedToWishlistAlert() {
        String alert = alteredWishlistAlert.getText();
        Assert.assertEquals("This product is added to your wishlist", alert);
    }

    public void removedFromWishlistAlert() {
        String alert = alteredWishlistAlert.getText();
        Assert.assertEquals("This product has been removed from your wishlist", alert);
    }

    @FindBy(css = ".m-plus")
    private WebElementFacade increaseQuantity;

    public void increaseQuantity(String number) {
        int numberInt = Integer.parseInt(number);
        for (int i = 1; i < (numberInt); i++) {
            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
            executor.executeScript("arguments[0].click();", increaseQuantity);
        }
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade quantityIncreased;

    public void checkIncreaseQuantity() {
        String alert = quantityIncreased.getText();
        Assert.assertEquals("Quantity changed", alert);
    }

    @FindBy(className = "b-product_wishlist-btn")
    private WebElementFacade addToWishlistButton;

    public void addToWishlistButton() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addToWishlistButton);
    }

    @FindBy(className = "b-header_wishlist-link")
    private WebElementFacade clickOnWishlistPage;

    public void clickOnWishlistPage() {
        clickOnWishlistPage.click();
    }

    @FindBy(className = "b-wishlist-items_count")
    private WebElementFacade checkItemInWishlistPage;

    public void checkItemInWishlistPage() {
        String alert = checkItemInWishlistPage.getText();
        Assert.assertEquals("1 Item", alert);
    }

    @FindBy(id = "dwfrm_evoucher_yourName")
    private WebElementFacade enterYourName;

    @FindBy(id = "dwfrm_evoucher_recepientName")
    private WebElementFacade enterRecipientName;

    @FindBy(id = "dwfrm_evoucher_recepientEmail")
    private WebElementFacade enterRecipientEmail;

    @FindBy(id = "dwfrm_evoucher_confirmRecepientEmail")
    private WebElementFacade confirmRecipientEmail;

    @FindBy(id = "dwfrm_evoucher_sendDate")
    private WebElementFacade enterDate;

    @FindBy(id = "dwfrm_evoucher_message")
    private WebElementFacade enterMessage;

    @FindBy(id = "eproduct-amounts-select")
    private WebElementFacade voucherAmountDropdown;

    @FindBy(xpath = "//*[@id=\"customSelect-wVdCz-panel\"]/div[6]")
    private WebElementFacade voucherAmount;


    public void evoucherDetails(String name, String recipientName, String recipientEmail, String date, String message) {

        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        Select dropdown = new Select(getDriver().findElement(By.id("eproduct-amounts-select")));
        dropdown.selectByVisibleText("£10.00");
        executor.executeScript("arguments[0].click();", enterYourName);
        enterYourName.sendKeys(name);
        executor.executeScript("arguments[0].click();", enterRecipientName);
        enterRecipientName.sendKeys(recipientName);
        executor.executeScript("arguments[0].click();", enterRecipientEmail);
        enterRecipientEmail.sendKeys(recipientEmail);
        executor.executeScript("arguments[0].click();", confirmRecipientEmail);
        confirmRecipientEmail.sendKeys(recipientEmail);
        executor.executeScript("arguments[0].click();", enterDate);
        enterDate.sendKeys(date);
        Select dropdown2 = new Select(getDriver().findElement(By.id("dwfrm_evoucher_subject")));
        dropdown2.selectByVisibleText("Merry Christmas! | e-Voucher");
        executor.executeScript("arguments[0].click();", enterMessage);
        enterMessage.sendKeys(message);
    }

    @FindBy(css = ".b-header_cart-count")
    private WebElementFacade itemsInBasketNumber;

    public void checkNumberItemsInBasket(String number) {
        String alert = itemsInBasketNumber.getText();
        Assert.assertEquals(number + " items in basket", alert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/main/section[1]/div[1]/section[2]/div[4]/div[5]/div/section/div[2]/div[2]/button")
    private WebElementFacade selectStoreButton;

    @FindBy(css = ".b-product_storelocator-button")
    private WebElementFacade useCurrentLocation;

    @FindBy(id = "dwfrm_selectStoreSearch_searchByPlace")
    private WebElementFacade enterPostcodeBox;

    @FindBy(xpath = "//*[@id=\"page-body\"]/ul[2]/li[1]")
    private WebElementFacade firstSelectionAddress;

    @FindBy(xpath = "//*[@id=\"510b8362865876f99dafc2a334\"]/div[3]/button")
    private WebElementFacade selectCorrectStore;

    public void selectStore(String postcode) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", selectStoreButton);
        executor.executeScript("arguments[0].click();", enterPostcodeBox);
        enterPostcodeBox.sendKeys(postcode);
        enterPostcodeBox.sendKeys(Keys.ENTER);
        executor.executeScript("arguments[0].click();", selectCorrectStore);
    }

    @FindBy(xpath = "//*[@id=\"howto\"]/div[2]/div[2]/div/div[2]/div/div/a/h3")
    private WebElementFacade viewProject;

    public void viewProject() {
        viewProject.click();
    }

    @FindBy(linkText = "Discover Related Ideas")
    private WebElementFacade discoverIdeasButton;

    @FindBy(id = "section-label-pdp-bottom-2")
    private WebElementFacade ideasForYouText;

    public void discoverIdeas() {
        discoverIdeasButton.click();
        String alert = ideasForYouText.getText();
        Assert.assertEquals("Ideas for you", alert);
    }

    @FindBy(linkText = "Cricut Rose Maker Ultimate Smart Cutting Machine")
    private WebElementFacade topProduct;

    @FindBy(xpath = "//*[@id=\"youwillneed\"]/div[7]/figure/figcaption/div[4]/div/label")
    private WebElementFacade addAProduct;

    @FindBy(xpath = "//*[@id=\"youwillneed\"]/div[6]/figure/figcaption/div[4]/div/label")
    private WebElementFacade addSecondProduct;

    @FindBy(css = ".b-product_addtocard_set")
    private WebElementFacade addSelectedProducts;

    @FindBy(xpath = "//*[@id=\"youwillneed\"]/div[10]/div[3]/button/span")
    private WebElementFacade addSelectedProductsToBasket;

    public void addIdeaProducts() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", topProduct);
        executor.executeScript("arguments[0].click();", addAProduct);
        executor.executeScript("arguments[0].click();", addSecondProduct);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(addSelectedProducts));
        executor.executeScript("arguments[0].scrollIntoView();", addSelectedProducts);
        executor.executeScript("arguments[0].click();", addSelectedProductsToBasket);
        executor.executeScript("arguments[0].click();", addSelectedProductsToBasket);
    }

    @FindBy(linkText = "Sign in to Download this pattern")
    private WebElementFacade signInToDownload;

    public void signInToDownload(){
        signInToDownload.click();
    }

    @FindBy(linkText = "Download this pattern")
    private WebElementFacade downloadItemButton;

    public void clickDownloadItem(){
        downloadItemButton.click();
    }
}
