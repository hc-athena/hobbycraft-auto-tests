package com.ten10.pages;

import lombok.var;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.lang.Float.parseFloat;

public class BasketPage extends PageObject {

    @FindBy(linkText = "Checkout")
    private WebElementFacade checkoutButton;

    public void selectCheckoutButton() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        waitForCondition().until(
                ExpectedConditions.elementToBeClickable(checkoutButton));
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", checkoutButton);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/div/section[1]/form/button")
    private WebElementFacade clickSignInAndCheckoutButton;

    public void selectSignInAndCheckoutButton() {
        clickSignInAndCheckoutButton.click();
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[1]/div[3]/div/div/div[2]/aside[2]/div/div/div[1]/button[1]")
    private WebElementFacade selectHomeDelivery;

    public void selectHomeDelivery() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", selectHomeDelivery);
    }

    public void selectHomeDeliveryAddCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", selectHomeDelivery);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[3]/div/div/div[2]/aside[2]/div/div/div[1]/button[2]")
    private WebElementFacade selectClickAndCollect;

    public void selectClickAndCollect() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", selectClickAndCollect);
    }

    @FindBy(xpath = "//*[@id=\"clickCollect\"]/section[2]/div[1]/button")
    private WebElementFacade clickFindAStore;

    public void clickFindAStore() {
        waitForCondition().until(
                ExpectedConditions.elementToBeClickable(clickFindAStore));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickFindAStore);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[3]/div/div/div[2]/main/div/div/p")
    private WebElementFacade checkSpendAlert;

    public void checkSpendAlert() {
        String alert = checkSpendAlert.getText();
        Assert.assertEquals("Minimum spend £10.00 for Click & Collect", alert);
    }


    @FindBy(id = "delivery-btn")
    private WebElementFacade deliveryButton;

    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * .b-coupon_form-title")
    private WebElementFacade addPromoCode;

    //    @FindBy(css = "#dwfrm_coupon_couponCode")
    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * #dwfrm_coupon_couponCode")
    private WebElementFacade typePromoCode;

    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * .m-active")
//    @FindBy(xpath = "//*[@id=\"coupon-form\"]/form/button")
    private WebElementFacade applyCode;

    public void addPromoCode(String promoCode) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        waitForCondition().until(
                ExpectedConditions.elementToBeClickable(checkoutButton));
        executor.executeScript("arguments[0].scrollIntoView();", deliveryButton);
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.elementToBeClickable(addPromoCode));
        executor.executeScript("arguments[0].click();", addPromoCode);
        typePromoCode.sendKeys(promoCode);
        executor.executeScript("arguments[0].click();", applyCode);
//        executor.executeScript("arguments[0].click();", addPromoCode);
    }

    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * #dwfrm_coupon_couponCode-error")
    private WebElementFacade promoCodeError;

    public void checkPromoCodeErrorMessage() {
        waitForCondition().until(ExpectedConditions.visibilityOf(promoCodeError));
        String alert = promoCodeError.getText();
        Assert.assertEquals("Promo code not recognised. Please retry or use a different code.", alert);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade removePromoCode;

    public void checkPromoApplied(){
        String alert = removePromoCode.getText();
        Assert.assertEquals("Promo code applied", alert);
    }

    public void checkCorrectItemsInBasket(String product_1, String product_2) {
        WebElement productLink1 = getDriver().findElement(By.linkText(product_1));
        WebElement productLink2 = getDriver().findElement(By.linkText(product_2));
        String product1 = productLink1.getText();
        Assert.assertEquals(product_1, product1);
        String product2 = productLink2.getText();
        Assert.assertEquals(product_2, product2);
    }

    @FindBy(className = "b-cart_product-remove")
    private WebElementFacade removeItem;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[4]/div/div/div[3]/button[2]")
    private WebElementFacade removeItemButton;

    public void removeItemFromBasket() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", removeItem);
        executor.executeScript("arguments[0].click();", removeItemButton);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade productRemovedAlert;

    public void productRemovedAlert() {
        String alert = productRemovedAlert.getText();
        Assert.assertEquals("Product removed", alert);
    }

    @FindBy(css = ".b-cart_quickview-button")
    private WebElementFacade continueShoppingButton;

    public void continueShopping() {
        continueShoppingButton.click();
    }

    @FindBy(className = "b-header_cart-count")
    private WebElementFacade itemsInBasketCount;

    public void itemsInBasket() {
        String alert = itemsInBasketCount.getText();
        Assert.assertEquals("2 items in basket", alert);
    }

}
