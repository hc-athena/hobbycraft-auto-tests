package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class CheckoutDetailsPage extends PageObject {

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_firstName")
    private WebElementFacade firstNameBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_lastName")
    private WebElementFacade lastNameBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_phone")
    private WebElementFacade phoneNumberBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_address1")
    private WebElementFacade address1Box;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_address2")
    private WebElementFacade address2Box;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_city")
    private WebElementFacade townBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_postalCode")
    private WebElementFacade postcodeBox;

    @FindBy(css = ".b-checkout_step-btn")
    private WebElementFacade saveAndContinue;

    public void deliveryDetailsFormFill(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstNameBox.clear();
        firstNameBox.sendKeys(first_name);
        lastNameBox.clear();
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.clear();
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
        address1Box.clear();
        address1Box.sendKeys(address_1);
        address2Box.clear();
        address2Box.sendKeys(address_2);
        townBox.clear();
        townBox.sendKeys(town);
        postcodeBox.clear();
        postcodeBox.sendKeys(postcode);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", saveAndContinue);
    }

    @FindBy(className = "b-option_switch-label_edit")
    private WebElementFacade editAddress;

    public void editAddress() {
        try {
            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
            executor.executeScript("arguments[0].click();", editAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FindBy(xpath = "//*[@id=\"payment-button-adyen\"]/span/span")
    private WebElementFacade payByCard;

    @FindBy(xpath = "//*[@id=\"payment-button-scheme\"]/span[2]/span")
    private WebElementFacade addANewCard;

    public void clickPayByCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", payByCard);
        try {
            executor.executeScript("arguments[0].click();", addANewCard);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FindBy(id = "encryptedCardNumber")
    private WebElementFacade cardNumberBox;

    @FindBy(id = "encryptedExpiryDate")
    private WebElementFacade expiryDateBox;

    @FindBy(id = "encryptedSecurityCode")
    private WebElementFacade securityCodeBox;

    @FindBy(className = "adyen-checkout__card__holderName__input")
    private WebElementFacade cardHolderNameBox;

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div[1]/main/section[2]/div[1]/div[2]/button")
    private WebElementFacade reviewOrderButton;


    public void paymentDetailsFormFill(String card_number, String expiry_date, String CVC, String name) {

        getDriver().switchTo().frame(0);
        cardNumberBox.sendKeys(card_number);
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame(1);
        expiryDateBox.sendKeys(expiry_date);
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame(2);
        securityCodeBox.sendKeys(CVC);
        getDriver().switchTo().defaultContent();
        cardHolderNameBox.sendKeys(name);
    }

    @FindBy(css = ".adyen-checkout__checkbox__label")
    private WebElementFacade saveCardTickBox;

    public void tickSaveCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", saveCardTickBox);
    }

    public void reviewOrder() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", reviewOrderButton);
    }

    @FindBy(id = "dwfrm_login_guestEmail")
    private WebElementFacade checkoutAsGuest;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/div/section[2]/form/button")
    private WebElementFacade clickCheckoutAsGuestButton;

    public void checkoutAsGuest(String email) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", checkoutAsGuest);
        checkoutAsGuest.sendKeys(email);
        clickCheckoutAsGuestButton.click();
    }

    @FindBy(xpath = "//*[@id=\"payment-button-PayPal\"]/span/span")
    private WebElementFacade payByPayPal;

    public void payByPal() {
        waitForCondition().until(
                ExpectedConditions.elementToBeClickable(payByPayPal));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", payByPayPal);
    }

    @FindBy(xpath = "//*[@id=\"payment-details-PayPal\"]/div/fieldset/div[1]")
    private WebElementFacade checkPayPalIsSelected;

    public void checkPayPalIsSelected() {
        String alert = checkPayPalIsSelected.getText();
        Assert.assertEquals("Your billing address and phone number will be changed according to the chosen payment method on PayPal side.", alert);
    }

    @FindBy(css = ".paypal-button-label-container")
    private WebElementFacade selectPayByPayPal;

    @FindBy(css = "iframe[class='component-frame visible']")
    private WebElementFacade framePaypalbutton;

    public void selectPayByPayPal() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("iframe[class='component-frame visible']")));
        getDriver().switchTo().frame(framePaypalbutton);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(selectPayByPayPal));
        executor.executeScript("arguments[0].scrollIntoView();", selectPayByPayPal);
        executor.executeScript("arguments[0].click();", selectPayByPayPal);
        executor.executeScript("arguments[0].click();", selectPayByPayPal);
        executor.executeScript("arguments[0].click();", selectPayByPayPal);
        getDriver().switchTo().defaultContent();
    }

    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * .paypal-button-label-container")
    private WebElementFacade selectPayByPayPalOnPDP;

    @FindBy(css = ".l-cart-aside:not(.m-mobile) > * #homeDelivery > * iframe[class='component-frame visible']")
    private WebElementFacade framePaypalbuttonOnPDP;

    public void selectPayByPayPalOnPDP() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".l-cart-aside:not(.m-mobile) > * #homeDelivery > * iframe[class='component-frame visible']")));
//        framePaypalbuttonOnPDP.click();
        getDriver().switchTo().frame(framePaypalbuttonOnPDP);
//        waitForCondition().until(ExpectedConditions.elementToBeClickable(selectPayByPayPal));
        executor.executeScript("arguments[0].scrollIntoView();", selectPayByPayPal);
        executor.executeScript("arguments[0].click();", selectPayByPayPal);
//        WebDriverWait hangOn = new WebDriverWait(getDriver(),30);
//        hangOn.until(ExpectedConditions.visibilityOf(emailPayPalBox));
//        executor.executeScript("arguments[0].click();", selectPayByPayPal);
//        executor.executeScript("arguments[0].click();", selectPayByPayPal);
        getDriver().switchTo().defaultContent();
    }

    @FindBy(id = "email")
    private WebElementFacade emailPayPalBox;

    @FindBy(id = "btnNext")
    private WebElementFacade nextPayPalButton;

    @FindBy(id = "password")
    private WebElementFacade passwordPayPalBox;

    @FindBy(id = "btnLogin")
    private WebElementFacade loginPayPalButton;

    @FindBy(id = "payment-submit-btn")
    private WebElementFacade continueButton;

    @FindBy(id = "acceptAllButton")
    private WebElementFacade acceptPayPalCookies;

    @FindBy(css = ".SpinnerOverlay_SpinnerMessage_1BLVf")
    private WebElementFacade processingPayPalMessage;

    public void loginToPayPal(String payPalUsername, String payPalPassword) {
        String currentWindow = getDriver().getWindowHandle();
        Set<String> allWindows = getDriver().getWindowHandles();
        for (String window : allWindows) {
            if (!window.contentEquals(currentWindow)) {
                getDriver().switchTo().window(window);
                break;
            }
        }
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        emailPayPalBox.sendKeys(payPalUsername);
        nextPayPalButton.click();
        passwordPayPalBox.sendKeys(payPalPassword);
        loginPayPalButton.click();
        executor.executeScript("arguments[0].click();", acceptPayPalCookies);
        executor.executeScript("arguments[0].click();", continueButton);

        waitForCondition().until(ExpectedConditions.textToBePresentInElement(processingPayPalMessage, "We're sending you back to"));
        getDriver().switchTo().window(currentWindow);
    }

    @FindBy(xpath = "//*[@id=\"payment-button-GiftCard\"]/span/span[1]")
    private WebElementFacade useGiftCard;

    public void clickUseGiftCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", useGiftCard);
    }

    @FindBy(id = "inputGiftCardNumber")
    private WebElementFacade enterGiftCardNumber;

    @FindBy(id = "inputGiftCardPin")
    private WebElementFacade enterPinNumber;

    @FindBy(xpath = "//*[@id=\"payment-details-GiftCard\"]/div/div/div[1]/button")
    private WebElementFacade useGiftCardButton;

    public void enterGiftCardNumber(String giftCardNumber, String pin) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", enterGiftCardNumber);
        enterGiftCardNumber.sendKeys(giftCardNumber);
        executor.executeScript("arguments[0].click();", enterPinNumber);
        enterPinNumber.sendKeys(pin);
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        waitForCondition().until(ExpectedConditions.elementToBeClickable(useGiftCardButton));
        executor.executeScript("arguments[0].click();", useGiftCardButton);
    }


    @FindBy(xpath = "//*[@id=\"payment-details-GiftCard\"]/div/div/button")
    private WebElementFacade checkGiftCardUsed;

    @FindBy(xpath = "//*[@id=\"payment-details-GiftCard\"]/div/div/button")
    private WebElementFacade addAnotherGiftCard;

    @FindBy(xpath = "//*[@id=\"payment-details-GiftCard\"]/div/div/div[2]/button")
    private WebElementFacade useGiftCardButtonForSecondCard;

    public void addAnotherGiftCard(String giftCardNumber, String pin){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.visibilityOf(giftCardDetails));
        executor.executeScript("arguments[0].click();", addAnotherGiftCard);
        executor.executeScript("arguments[0].click();", enterGiftCardNumber);
        enterGiftCardNumber.sendKeys(giftCardNumber);
        executor.executeScript("arguments[0].click();", enterPinNumber);
        enterPinNumber.sendKeys(pin);
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        waitForCondition().until(ExpectedConditions.elementToBeClickable(useGiftCardButtonForSecondCard));
        executor.executeScript("arguments[0].click();", useGiftCardButtonForSecondCard);
    }

    @FindBy(css = ".b-giftcard-details")
    private WebElementFacade giftCardDetails;

    public void giftCardDetails(){
        String alert = giftCardDetails.getText();
        Assert.assertEquals("Your gift card or e-voucher ****-******-3371 applied, £5.00 has been applied to your order. Remove", alert);
    }

    public void collectionDetails(String first_name, String last_name, String phone_number) {
        firstNameBox.sendKeys(first_name);
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
    }

    public void saveAndContinue() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", saveAndContinue);
        saveAndContinue.click();
    }

    @FindBy(linkText = "Change Address")
    private WebElementFacade changeAddressLink;

    @FindBy(xpath = "//*[@id=\"dwfrm_billing\"]/div[2]/fieldset[1]/fieldset/div[1]/button")
    private WebElementFacade addNewAddressOption;

    public void changeBillingAddress() {
        try {
            changeAddressLink.click();
            addNewAddressOption.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FindBy(id = "dwfrm_billing_addressFields_firstName")
    private WebElementFacade firstNameBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_lastName")
    private WebElementFacade lastNameBillingBox;

    @FindBy(id = "dwfrm_billing_contactInfoFields_phone")
    private WebElementFacade phoneNumberBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_address1")
    private WebElementFacade address1BillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_address2")
    private WebElementFacade address2BillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_city")
    private WebElementFacade townBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_postalCode")
    private WebElementFacade postcodeBillingBox;

    @FindBy(xpath = "//*[@id=\"dwfrm_billing_addressFields_country\"]/option[236]")
    private WebElementFacade unitedKingdomCountryOption;

    public void billingDetailsFormFill(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstNameBillingBox.clear();
        firstNameBillingBox.sendKeys(first_name);
        lastNameBillingBox.clear();
        lastNameBillingBox.sendKeys(last_name);
        phoneNumberBillingBox.clear();
        phoneNumberBillingBox.sendKeys(String.valueOf(phone_number));
        Select dropdown = new Select(getDriver().findElement(By.id("dwfrm_billing_addressFields_country")));
        dropdown.selectByVisibleText("United Kingdom");
        address1BillingBox.clear();
        address1BillingBox.sendKeys(address_1);
        address2BillingBox.clear();
        address2BillingBox.sendKeys(address_2);
        townBillingBox.clear();
        townBillingBox.sendKeys(town);
        postcodeBillingBox.clear();
        postcodeBillingBox.sendKeys(postcode);
        waitFor(5000);
    }

//    @FindBy(xpath = "//*[@id=\"dwfrm_profile_customer_agreeToPrivacy\"]")
    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[3]/div/div[1]/div/div/div[2]/div[1]/label")
    private WebElementFacade acceptTerms;

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[3]/div/div[2]/button/span")
    private WebElementFacade placeOrder;

    public void completeOrder() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", reviewOrderButton);
        waitForCondition().until(ExpectedConditions.elementToBeClickable(acceptTerms));
        executor.executeScript("arguments[0].click();", acceptTerms);
        executor.executeScript("arguments[0].click();", placeOrder);
    }

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[2]/div[2]/div/div/div[1]/h3")
    private WebElementFacade billingAddressTitle;

    public void completeOrderForPayPal() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        waitForCondition().until(ExpectedConditions.visibilityOf(billingAddressTitle));
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", acceptTerms);
        executor.executeScript("arguments[0].click();", placeOrder);
    }

    @FindBy(className = "b-confirmation_header-title")
    private WebElementFacade confirmationOrder;

    public void confirmationOfOrder() {
        WebDriverWait wait = new WebDriverWait(getDriver(),30);
        wait.until(ExpectedConditions.visibilityOf(confirmationOrder));
        String alert = confirmationOrder.getText();
        Assert.assertEquals("Thank you for your order", alert);
    }

    @FindBy(className = "b-confirmation_header-title")
    private WebElementFacade confirmationOrderForClickAndCollect;

    public void confirmationOfOrderForClickAndCollect() {
        String alert = confirmationOrderForClickAndCollect.getText();
        Assert.assertEquals("Thank you for ordering", alert);
    }

    @FindBy(css = ".b-giftcard-balance_value")
    private WebElementFacade outstandingBalance;

    @FindBy(id = "savedGiftCards")
    private WebElementFacade giftCardUsed;

    public void checkOutstandingBalance(String balance) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.visibilityOf(giftCardUsed));
        String outstandingBalanceCheck = outstandingBalance.getText();
        Assert.assertEquals("£" + balance, outstandingBalanceCheck);

    }
}
