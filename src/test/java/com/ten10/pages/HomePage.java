package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class HomePage extends PageObject {

    @FindBy(className = "b-header_login-caption")
    private WebElementFacade signInLink;

    public void clickSignInLink() {
        signInLink.click();
    }

    @FindBy(css = ".osano-cm-accept-all")
    private WebElementFacade acceptCookiesButton;

    public void clickAcceptCookies() {
        acceptCookiesButton.click();
    }

    @FindBy(className = "b-search_toggle")
    private WebElementFacade searchBarButton;

    public void clickSearchBarButton() {
        searchBarButton.click();
    }

    @FindBy(id = "header-search-input")
    private WebElementFacade searchBar;

    public void searchForProduct(String product) {
        searchBar.sendKeys(product);
    }

    @FindBy(className = "b-search_input-submit")
    private WebElementFacade enterSearchButton;

    public void enterSearch() {
        enterSearchButton.click();
    }

    @FindBy(linkText = "Stores")
    private WebElementFacade clickStores;

    public void clickStores() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickStores);
    }

    @FindBy(linkText = "Workshops")
    private WebElementFacade clickWorkshops;

    public void clickWorkshops() {
        clickWorkshops.click();
    }

    @FindBy(linkText = "FAQs")
    private WebElementFacade clickOnFAQs;

    public void clickOnFAQs() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", clickOnFAQs);
        clickOnFAQs.click();
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/section/div/h2")
    private WebElementFacade thisWeeksDeals;

    public void checkOnHomePage() {
        String alert = thisWeeksDeals.getText();
        Assert.assertEquals("Our best offers", alert);
    }

    @FindBy(xpath = "//*[@id=\"idea-carousel\"]/section/div[2]/div[2]/div/div[1]/section/div[2]/button/span")
    private WebElementFacade wishlistRecommended;

    public void wishlistRecommended() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", wishlistRecommended);
    }

//    @FindBy(css = ".b-wishlist-icon")
    @FindBy(xpath = "//*[@id=\"idea-carousel\"]/section/div[2]/div[2]/div/div[1]/section/div[1]/a/picture/img")
    private WebElementFacade homePageIdea;

    @FindBy(css = ".b-product_wishlist-btn")
    private WebElementFacade addToWishlist;

    public void wishListFromIdea() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", homePageIdea);
        executor.executeScript("arguments[0].click();", homePageIdea);
        executor.executeScript("arguments[0].click();", addToWishlist);
    }

    public void chooseCategory(String category, String sub_category) {
        WebElement categoryLink = getDriver().findElement(By.linkText(category));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", categoryLink);
        WebElement subCategoryLink = getDriver().findElement(By.linkText(sub_category));
        executor.executeScript("arguments[0].click();", subCategoryLink);
        executor.executeScript("arguments[0].click();", subCategoryLink);
    }

    @FindBy(css = ".b-country_selector-icon")
    private WebElementFacade currentCountry;

    @FindBy(css = ".glSaveBtn")
    private WebElementFacade saveCountryButton;

    public void selectCountry(String country) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", currentCountry);
        executor.executeScript("arguments[0].click();", currentCountry);
        waitForCondition().until(ExpectedConditions.visibilityOf(saveCountryButton));
        Select dropdown = new Select(getDriver().findElement(By.id("gle_selectedCountry")));
        dropdown.selectByVisibleText(country);
        executor.executeScript("arguments[0].click();", saveCountryButton);
    }

    @FindBy(css = ".backToShop")
    private WebElementFacade continueShopping;

    @FindBy(css = ".b-country_selector-label")
    private WebElementFacade shipToCountry;

    public void checkCountry(String shippingCountry) {
        waitForCondition().until(ExpectedConditions.visibilityOf(continueShopping));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", continueShopping);
        String countryLabel = shipToCountry.getText();
        Assert.assertEquals(shippingCountry, countryLabel);
    }

    public void scrollToBottom() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", currentCountry);
    }

    @FindBy(css = ".b-back_to_top-copy_large")
    private WebElementFacade backToTop;

    public void clickBackToTop() {
        backToTop.click();
    }


    @FindBy(xpath = "//*[@id=\"idea-carousel\"]/section/div[2]/div[1]/div/div[2]/section/div[2]/p/a")
    private WebElementFacade recommendedIdea;

    public void clickRecommendedIdea() {
        recommendedIdea.click();
    }

    @FindBy(linkText = "View All Ideas")
    private WebElementFacade viewAllIdeas;

    public void viewAllIdeas() {
        viewAllIdeas.click();
    }

}

