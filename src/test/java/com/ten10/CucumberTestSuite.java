package com.ten10;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        tags = "not @Ignore"
)
//For tags; tags = "@SmokeTest" will run all scenarios tagged as @SmokeTest
//Can also do tags = "not @ignore" to run all tests NOT tagged with @ignore
public class CucumberTestSuite {}
