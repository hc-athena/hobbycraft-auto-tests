package com.ten10.utils;

import java.util.UUID;

public class EmailGenerator {


//    private static String EMAIL_ID_PREFIX = "HCAuto";

        public static String getUniqueId() {
            return String.format("%s_%s", UUID.randomUUID().toString().substring(0, 5), System.currentTimeMillis() / 1000);
        }

        public static String generateEmail(String EMAIL_ID_PREFIX) {
            return String.format("%s_%s@%s", EMAIL_ID_PREFIX, getUniqueId(), "stub.me.uk");
        }

        public static void main(String[] args) {
            System.out.println(generateEmail("Hello"));
        }

        }
